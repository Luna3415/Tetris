﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Tetris_v01
{
    static class Bestandenverwerker
    {
        private const string TetrisBestand = "Tetris.txt";

        //Controleert of de leerling zijn/haar highscore van tetris verbeterd heeft
        //Als de score verbeterd werd, dan wordt het bestandje met de score overschreven
        //Datum: 09/05/2015
        //Auteur: Kim Gijbels
        /// <summary>
        /// Past indien nodig het bestandje met de higscore van tetris aan
        /// </summary>
        /// <param name="score"></param>
        public static void ControleHogereTetrisHighscore(uint score)
        {
            string path =  Path.Combine( "..\\..\\saves", TetrisBestand);
            List<string> tetrisScore = LeesBestand(path);
            List<string> scoreList = new List<string>();

            scoreList.Add(Convert.ToString(score));
            try
            {
                if (tetrisScore.Count == 0)
                {
                    SchrijfBestand(path, scoreList);
                }
                else if (UInt32.Parse(tetrisScore[0]) < score)
                {
                    SchrijfBestand(path, scoreList);
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Het highscore bestand is corrupt, mag alleen cijfers bevatten!");
            }
            catch (Exception)
            {
                MessageBox.Show("Fout bij het inlezen van het highscore bestand, gelieve het terug te zetten naar de oorspronkelijke staat!");
            }
        }

        //Leest de tetrishighScore van de leerling uit en geeft deze terug
        //Datum: 09/05/2015
        //Auteur: Kim Gijbels
        /// <summary>
        /// Vraagt de Tetris highscore op uit het bestand
        /// </summary>
        /// <returns>Tetris highscore</returns>
        public static uint GetTetrisHighScore()
        {
            string pad = Path.Combine("..\\..\\saves", TetrisBestand);

            List<string> score = LeesBestand(pad);

            try
            {
                return Convert.ToUInt32(score[0]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static List<string> LeesBestand(string pad)
        {
            List<string> lijst = new List<string>();
            StreamReader inputStream = null;

            try
            {
                FileStream fstream = new FileStream(pad, FileMode.Open, FileAccess.Read);
                inputStream = new StreamReader(fstream);

                string lijn = inputStream.ReadLine();

                while (lijn != null)
                {
                    lijst.Add(lijn);
                    lijn = inputStream.ReadLine();
                }
                fstream.Close();
                fstream.Dispose();
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show(
                        "Kan de highscore niet uitlezen! Bestand ontbreekt!");
            }
            catch (Exception)
            {
                MessageBox.Show("Er is een fout opgetreden bij het inlezen van de highscore!");
            }
            finally
            {
                if (inputStream != null)
                {
                    inputStream.Close();
                }
            }
            return lijst;
        }

        private static void SchrijfBestand(string pad, List<string> regels)
        {
            StreamWriter writer = null;
            try
            {
                FileStream fstream = new FileStream(pad, FileMode.Create, FileAccess.Write);
                writer = new StreamWriter(fstream);

                foreach (string regel in regels)
                {
                    writer.WriteLine(regel);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }
    }
}
