﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Tetris_v01
{
    class Blokjes
    {
        private List<Rectangle> blokjes = new List<Rectangle>(); //http://stackoverflow.com/questions/3367524/c-sharp-objects-in-arraylists
        private static double zijdeLengte = 30;     //Omdat de hoogte gelijk is aan de breedte bij een vierkant, static omdat dit voor ieder object van deze klasse hetzelfde gaat zijn
        private int hoogsteYBlokje;
        private int laagsteYBlokje;
        private int hoogsteXBlokje;
        private int laagsteXBlokje;
        private double[] rotatieCoord = new double[2];

        //Maakt de blokjes aan en positioneert de blokjes in een vooraf bepaalde tetrisvorm
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public Blokjes(int tetrisVorm, double xPos, double yPos, SolidColorBrush brush)
        {
            for (int i=0; i<4; i++)     //4 Blokjes nodig
            {
                blokjes.Add(new Rectangle());
                blokjes[i].Height = zijdeLengte;
                blokjes[i].Width = zijdeLengte;
            }
            BlokjesCombineren(tetrisVorm, xPos, yPos, brush);
            GetExtremePosBlokjes();
        }

        //Property om op te vragen uit hoeveel blokjes de tetromino bestaat (kan wijzigen, waneer er rijen uit de grid verwijdert worden)
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public int AantalBlokjes
        {
            get { return blokjes.Count; }
        }

        //Property voor de lengte van de zijde op te vragen en/of te wijzigen
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public static double Zijde
        {
            get { return zijdeLengte; }
            set { zijdeLengte = value; }
        }

        //Property om de ypositie van het laagsthangende blokje door te geven (hoogste y waarde)
        //Date: 27/03/2015 
        //Auteur: Kim Gijbels
        public double LaagstHangendYPos
        {
            get { return blokjes[hoogsteYBlokje].Margin.Top; }
        }

        //Property om van het meest linkse blokje de x waarde mee te geven
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public double LinkseBlokXPos
        {
            get { return blokjes[laagsteXBlokje].Margin.Left; }
        }

        //Property om van het meest rechtse blokje de x waarde mee te geven
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public double RechtseBlokXPos
        {
            get { return blokjes[hoogsteXBlokje].Margin.Left; }
        }

        //Geeft de index van het laagst hangende blokje mee (blokje met de hoogste y waarde)
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public int IndexLaagsteBlokje
        {
            get { return hoogsteYBlokje; }
        }

        //Geeft de index van het hoogste blokje mee (blokje met de laagste y waarde)
        //Date 29/03/2015
        //Auteur: Kim Gijbels
        public int IndexHoogsteBlokje
        {
            get { return laagsteYBlokje; }
        }

        //Geeft de index van het meest rechtse blokje mee (blokje met de hoogste x waarde)
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public int IndexRechtsteBlokje
        {
            get { return hoogsteXBlokje; }
        }

        //Geeft de index van het meest linkse blokje mee (blokje met de laagste x waarde)
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public int IndexLinksteBlokje
        {
            get { return laagsteXBlokje; }
        }

        //Geeft de x waarde van het rotatiepunt door, nodig om de rotatie uit te voeren
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public double XRotationPoint
        {
            get { return rotatieCoord[1]; }
        }

        //Geeft de y waarde van het rotatiepunt door, nodig om de rotatie uit te voeren
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public double YRotationPoint
        {
            get { return rotatieCoord[0]; }
        }

        //Functie waarmee een bepaalt blokje kan opgevraagd worden
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public Rectangle GetBlokje(int indexBlokje)
        {
            return blokjes[indexBlokje];
        }

        //Vraagt de y coordinaat van een blokje op
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public double GetYPosBlokje(int blokje)
        {
            return blokjes[blokje].Margin.Top;
        }

        //Vraagt de x coordinaat van een blokje op
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public double GetXPosBlokje(int blokje)
        {
            return blokjes[blokje].Margin.Left;
        }

        //Berekent de y positie van het blokje relatief ten opzichte van de y waarde van het rotatiepunt, dit is nodig om het blokje te kunnen roteren
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public double GetYPosRelToRotatiePunt(int blokje)
        {
            return blokjes[blokje].Margin.Top + (zijdeLengte / 2) - rotatieCoord[0];
        }

        //Berekent de x positie van het blokje relatief ten opzichte van de x waarde van het rotatiepunt, dit is nodig om het blokje te kunnen roteren
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public double GetXPosRelToRotatiePunt(int blokje)
        {
            return blokjes[blokje].Margin.Left + (zijdeLengte / 2) - rotatieCoord[1];
        }

        //Update van positie van het blokje nodig voor de nieuwe verworven positie door rotatie aan het blokje mee te geven
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public void UpdatePosBlokje(int blokje, Thickness positie)
        {
            blokjes[blokje].Margin = positie;
        }

        //Verwijdert het blokje van de canvas ten gepaste tijden
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void VerwijderBlokje(int blokje, Canvas gridCanvas)
        {
            gridCanvas.Children.Remove(blokjes[blokje]);
        }

        //Het blokje moet ook uit de lijst verwijdert worden, zodat het niet meer toegangkelijk gemaakt wordt
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void VerwijderBlokjesUitList(List<int> verwijderbareBlokjes)
        {
            verwijderbareBlokjes.Sort();

            for (int blokje=verwijderbareBlokjes.Count-1; blokje>=0; blokje--)
            {
                blokjes.RemoveAt(verwijderbareBlokjes[blokje]);
            }
        }

        //Incrementeert de ywaarde van de blokjes met de doorgevoerde waarde (zou de zijdelengte zijn)
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void SetTopMarginBlokje(int blokje, Thickness positie)
        {
            Thickness margin = blokjes[blokje].Margin;
            margin.Top += positie.Top;
            blokjes[blokje].Margin = margin;
        }

        //Zorgt ervoor dat het rotatiepunt mee naar beneden verplaatst als het blokje zich naar beneden verplaatst
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public void UpdateRotatieYCoord()
        {
            rotatieCoord[0] += zijdeLengte;
        }

        //Zorgt ervoor dat het rotatiepunt zich mee horizontaal verplaatst als het blokje zich horizontaal verplaatst (richting is 1 of -1 -> links bewegen = negatief)
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public void UpdateRotatieXCoord(int richting)
        {
            rotatieCoord[1] += zijdeLengte * richting;
        }

        //Positioneert de blokjes in een vooraf bepaalde vorm tetrisvorm bepaalt dit
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void BlokjesCombineren(int tetrisvorm, double xPos, double yPos, SolidColorBrush brush)
        {
            switch (tetrisvorm)
            {
                //De lange tetromino, horizontaal getekend
                case 0:
                    blokjes[0].Margin = new Thickness(xPos, yPos, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (3 * zijdeLengte), yPos, 0, 0);
                    rotatieCoord[0] = blokjes[1].Margin.Top + zijdeLengte;
                    rotatieCoord[1] = blokjes[1].Margin.Left + zijdeLengte;
                    break;
                //Tetromino in de vorm van een J, horizontaal getekend
                case 1:
                    blokjes[0].Margin = new Thickness(xPos, yPos, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos, yPos + zijdeLengte, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos + zijdeLengte, 0, 0);
                    rotatieCoord[0] = blokjes[2].Margin.Top + (zijdeLengte / 2);
                    rotatieCoord[1] = blokjes[2].Margin.Left + (zijdeLengte / 2);
                    break;
                //Tetromino L, horizontaal getekend
                case 2:
                    blokjes[0].Margin = new Thickness(xPos, yPos + zijdeLengte, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos + zijdeLengte, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos, 0, 0);
                    rotatieCoord[0] = blokjes[1].Margin.Top + (zijdeLengte / 2);
                    rotatieCoord[1] = blokjes[1].Margin.Left + (zijdeLengte / 2);
                    break;
                //De vierkante tetromino
                case 3:
                    blokjes[0].Margin = new Thickness(xPos, yPos, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos, yPos + zijdeLengte, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    rotatieCoord[0] = blokjes[0].Margin.Top + zijdeLengte;
                    rotatieCoord[1] = blokjes[0].Margin.Left + zijdeLengte;
                    break;
                //De tetromino in de vorm van een s
                case 4:
                    blokjes[0].Margin = new Thickness(xPos, yPos + zijdeLengte, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + zijdeLengte, yPos, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos, 0, 0);
                    rotatieCoord[0] = blokjes[1].Margin.Top + (zijdeLengte / 2);
                    rotatieCoord[1] = blokjes[1].Margin.Left + (zijdeLengte / 2);
                    break;
                //De tetromino in de vorm van een E
                case 5:
                    blokjes[0].Margin = new Thickness(xPos, yPos + zijdeLengte, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + zijdeLengte, yPos, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos + zijdeLengte, 0, 0);
                    rotatieCoord[0] = blokjes[1].Margin.Top + (zijdeLengte / 2);
                    rotatieCoord[1] = blokjes[1].Margin.Left + (zijdeLengte / 2);
                    break;
                //Laatste tetromino
                default:
                    blokjes[0].Margin = new Thickness(xPos, yPos, 0, 0);
                    blokjes[1].Margin = new Thickness(xPos + zijdeLengte, yPos, 0, 0);
                    blokjes[2].Margin = new Thickness(xPos + zijdeLengte, yPos + zijdeLengte, 0, 0);
                    blokjes[3].Margin = new Thickness(xPos + (2 * zijdeLengte), yPos + zijdeLengte, 0, 0);
                    rotatieCoord[0] = blokjes[2].Margin.Top + (zijdeLengte / 2);
                    rotatieCoord[1] = blokjes[2].Margin.Left + (zijdeLengte / 2);
                    break;
            }
            KleurTetrisvorm(brush);
        }

        //Kleurt de blokjes die de tetrisvorm vormen in een kleur, typerend voor het vormpje
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        private void KleurTetrisvorm(SolidColorBrush color)
        {
            for (int i = 0; i < blokjes.Count; i++)
            {
                blokjes[i].Fill = color;
            }
        }

        /*Berekent de 4 uithoeken van een tetrisvormpje, zal nadien gebruikt worden om niet alle blokjes hun positie te moeten testen
          Maar dus maar een paar, die er echt toe doen. Dit wordt berekent van zodra dat een tetrisvorm gegenereerd wordt en wanneer het blokje geroteerd wordt */
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        public void GetExtremePosBlokjes()
        {
            hoogsteYBlokje = GetHoogsteYPosBlokje();
            laagsteYBlokje = GetLaagsteYPosBlokje();
            hoogsteXBlokje = GetHoogsteXPosBlokje();
            laagsteXBlokje = GetLaagsteXPosBlokje();
        }

        //Zoekt het blokje in het tetrisvormpje met de hoogste y waarde, methode geeft de index van het blokje terug
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        private int GetHoogsteYPosBlokje()
        {
            double hoogste = blokjes[0].Margin.Top;
            int blokjeHoogstY = 0;

            for (int i = 1; i < blokjes.Count; i++)
            {
                if (blokjes[i].Margin.Top > hoogste)
                {
                    hoogste = blokjes[i].Margin.Top;
                    blokjeHoogstY = i;
                }
            }
            return blokjeHoogstY;
        }

        //Zoekt het blokje in het tetrisvormpje met de laagste y waarde, functie geeft de index van dit blokje terug
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        private int GetLaagsteYPosBlokje()
        {
            double laagste = blokjes[0].Margin.Top;
            int blokjeLaagsteY = 0;

            for (int i = 1; i < blokjes.Count; i++)
            {
                if (blokjes[i].Margin.Top < laagste)
                {
                    laagste = blokjes[i].Margin.Top;
                    blokjeLaagsteY = i;
                }
            }
            return blokjeLaagsteY;
        }

        //Zoekt het blokje met de hoogste x waarde in het tetrisvormpje, functie geeft de index van het desbetreffende blokje terug
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        private int GetHoogsteXPosBlokje()
        {
            double hoogste = blokjes[0].Margin.Left;

            int blokjeHoogstX = 0;
            for (int i = 1; i < blokjes.Count; i++)
            {
                if (blokjes[i].Margin.Left > hoogste)
                {
                    hoogste = blokjes[i].Margin.Left;
                    blokjeHoogstX = i;
                }
            }
            return blokjeHoogstX;
        }

        //Zoekt het blokje met de laagste x waarde in het tetrisvormpje, functie geeft de index van het desbetreffende blokje terug
        //Date: 29/03/2015
        //Auteur: Kim Gijbels
        private int GetLaagsteXPosBlokje()
        {
            double laagste = blokjes[0].Margin.Left;

            int blokjeLaagstetX = 0;
            for (int i = 1; i < blokjes.Count; i++)
            {
                if (blokjes[i].Margin.Left < laagste)
                {
                    laagste = blokjes[i].Margin.Left;
                    blokjeLaagstetX = i;
                }
            }
            return blokjeLaagstetX;
        }
    }
}
