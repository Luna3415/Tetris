﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Tetris_v01
{
    //Class die het hele tetrisspel in gang zet en controleert
    //Date: 27/03/2015 12:30
    //Auteur: Kim Gijbels
    class TetrisSpel
    {
        private SolidColorBrush[] brushes = new SolidColorBrush[7];
        private int verwijderdeLijnen;

        private List<bool[]> grid = new List<bool[]>();
        //Een lijst van arrays maakt het veel simpeler om later de volledige rijen te kunnen verwijderen en er nieuwe van bovenaf in te schuiven

        private Canvas gridCanvas;
        private Label highscoreLabel;
        private Tetrisvorm huidigeTetromino;

        private const double InitialX = 90;
        //Gebruikt voor de nieuwe tetromino's te laten tevoorschijn komen op deze X coordinaat

        private const double InitialY = 0;
        //Gebruikt voor de nieuwe tetromino's te laten tevoorschijn komen op deze Y coordinaat

        private int level = 1;
        private Label levelLabel;
        private Thickness positieVeranderingTetromino; //Gebruikt om de vallende tetrominos hun coördinaten te updaten
        private Random randomGetalGen;
        private uint score;
        private Label scoreLabel;
        private int timingInterval = 1000;
        private DispatcherTimer valTijd = new DispatcherTimer();
        private List<Tetrisvorm> vorigeTetrominoes = new List<Tetrisvorm>();
        private bool[] vulArray; //Gebruikt om Grid te initialiseren en de lines te clearen
        private MainWindow window; //Nodig om later door te geven aan het Hoofdscherm

        //Constructor voor het tetris spel, vraagt de window op, gridcanvas, levelLabel, scoreLabel en highscore label
        //Date: 27/03/2015 12:35
        //Auteur: Kim Gijbels
        public TetrisSpel(Canvas gridCanvas, Label levelLabel, Label scoreLabel, Label highscoreLabel, MainWindow window)
        {
            randomGetalGen = new Random(new Random().Next());
            //Initialiseert de randomgetal generator met een random toevalszaadje
            this.window = window;
            window.KeyDown += window_KeyDown;

            this.gridCanvas = gridCanvas;
            this.levelLabel = levelLabel;
            this.highscoreLabel = highscoreLabel;
            this.scoreLabel = scoreLabel;

            InitialiseerGrid();
            DrawEndingLineOnCanvas();
            TekenBoordRondCanvas();

            this.highscoreLabel.Content = string.Format("{0:D7}", Bestandenverwerker.GetTetrisHighScore());

            positieVeranderingTetromino = new Thickness(InitialX, InitialY, 0, 0);

            SetBrushColors();
            GenereerRandomTetromino();
            valTijd.Interval = TimeSpan.FromMilliseconds(timingInterval);
            valTijd.Tick += valTijd_Tick;
        }


        //http://stackoverflow.com/questions/3662842/how-do-events-cause-memory-leaks-in-c-sharp-and-how-do-weak-references-help-miti
        //Datum: 09/05/2015 14:27
        //Auteur: Kim Gijbels
        /// <summary>
        /// Sluit alle openstaande eventhandlers af en schrijft de score weg naar het bestand
        /// </summary>
        public void Afsluiten()
        {
            Bestandenverwerker.ControleHogereTetrisHighscore(Convert.ToUInt32(highscoreLabel.Content));
            valTijd.Stop();
            window.KeyDown -= window_KeyDown;
            /*Mag absoluut niet vergeten worden, want dit zorgt ervoor dat de garbage collection het vorige object kan opruimen.
         Moest dit niet gebeuren, dan is de kans reëel dat mainwindow eigenlijk het vorige tetrisspel gebruikt ipv het huidige.
         Het losmaken van eventhandlers is essentieel om reference problemen te vermijden*/
        }

        //Controleert of dat het huidige blokje over de rand van canvas zou gaan of tegen een ander blokje zou aanbotsen
        //indien het nergens tegen aan botst, dan verandert de positie van de tetromino.
        //Date: 28/03/2015 11:45
        //Auteur: Kim Gijbels
        /// <summary>
        /// Beweegt het blokje horizontaal met de nodige controles.
        /// Richting: -1 = links, 1 = rechts
        /// </summary>
        /// <param name="richting"></param>
        private void BeweegHorizontaal(int richting)
        {
            if (!IsVolgendePosOverCanvas(richting) && !IsVolgendePosOverlapHor(richting))
            {
                positieVeranderingTetromino.Left = Blokjes.Zijde * richting;
                huidigeTetromino.SetLeftMarginTetromino(positieVeranderingTetromino);
                huidigeTetromino.Tetromino.UpdateRotatieXCoord(richting);
            }
        }

        //Vooraleer het tetrisblokje een aantal waarden naar beneden te verplaatsen, wordt er gecontroleerd of het blokje geen andere blokjes raakt of eigenlijk al op de grond staat
        //Date: 27/03/2015 13:09
        //Auteur: Kim Gijbels
        /// <summary>
        /// Laat het blokje naar beneden bewegen inclusief de benodigde controles
        /// Als het blokje niet voorbij de controles geraakt, dan wordt de valtijd gestopt, het grid gevuld, tetromino toegevoegd aan lijst 
        /// met vorige tetrominos, grid wordt gecontroleerd op volledige rijen (en indien nodig wordt er actie ondernomen).
        /// Tenslotte wordt er ook een nieuwe tetromino gegenereerd 
        /// </summary>
        private void BeweegNaarBeneden()
        {
            if (!IsBlokjeOpGrond() && !IsVolgendePosOverlap())
            {
                positieVeranderingTetromino.Top = Blokjes.Zijde;                //elk blokje zal met zijn eigen lengte naar beneden verplaatsen 
                huidigeTetromino.SetTopMarginTetromino(positieVeranderingTetromino);
                huidigeTetromino.Tetromino.UpdateRotatieYCoord();
            }
            else
            {
                valTijd.Stop();
                VullenGrid();
                vorigeTetrominoes.Add(huidigeTetromino);
                CheckenRijenGrid();
                GenereerRandomTetromino();
            }
        }

        //Roteert de blokjes tegenwijzerszin, eerst wordt er gecontroleerd of de blokjes over de rand van de canvas zouden gaan als ze geroteerd zouden worden
        //Als dit niet het geval is wordt er voor elk blokje een berekening gemaakt wat de nieuwe coördinaat zou zijn indien er geroteerd wordt, daarna wordt er
        //nagegaan ofdat deze nieuwe coördinaten niet overlappen met een huidig blokje op het speelveld. Overlapt er niets dan wordt de rotatie uitgevoerd en
        //worden de extreme posities van het het huidige blokje opnieuw berekend 
        //Date: 29/03/2015 20:15
        /// <summary>
        /// Roteert de tetromino en voert hierbij de cruciale controles uit
        /// </summary>
        private void RoteerTetromino()
        {
            double x1, y1;
            bool rotate = true;
            Thickness[] rotatieCoordinaten = new Thickness[4];

            if (!IsRotatieOverCanvas())
            {
                for (int i = 0; i < huidigeTetromino.Tetromino.AantalBlokjes; i++)
                {
                    x1 = -huidigeTetromino.Tetromino.GetYPosRelToRotatiePunt(i);
                    y1 = huidigeTetromino.Tetromino.GetXPosRelToRotatiePunt(i);
                    rotatieCoordinaten[i].Left = (huidigeTetromino.Tetromino.XRotationPoint + x1) -
                                                 (Blokjes.Zijde / 2);
                    rotatieCoordinaten[i].Top = (huidigeTetromino.Tetromino.YRotationPoint + y1) -
                                                (Blokjes.Zijde / 2);

                    //Als er ook maar 1 blokje door de rotatie op een ander blokje komt, gaat de rotatie niet door
                    if (grid[(int)rotatieCoordinaten[i].Top / (int)Blokjes.Zijde][(int)rotatieCoordinaten[i].Left / (int)Blokjes.Zijde])
                    {
                        rotate = false;
                    }
                }
                if (rotate)
                {
                    VoerRotatieUit(rotatieCoordinaten);
                }
            }
        }

        //Controleert of er in het tetrisspel volle rijen zijn, indien dit het geval is wordt er een functie aangeroepen die de volle rijen verwijdert 
        //Date: 27/03/2015 15:10
        //Auteur: Kim Gijbels
        /// <summary>
        /// Checkt of er rijen vol zijn, als er volle rijen zijn, worden er methodes aangroepen die dit verder afhandelen
        /// </summary>
        private void CheckenRijenGrid()
        {
            bool[] completeRijen = new bool[grid.Count];

            completeRijen = InitialiseertCompleteRijen(completeRijen);
            completeRijen = DetecteertOncompleteRijen(completeRijen);
            ControleCompleteRijen(completeRijen);
        }

        //Als er volle rijen zijn, roep dan de methode op die de complete rijen zal verwijderen
        //Date: 06/04/2015 16:03
        //Auteur: Kim Gijbels
        /// <summary>
        /// Doet de eigenlijke controle: wanneer de complete rijen array al ingevuld werd
        /// Indien, er een volle rij is, wordt de methode aangeroepen die de volledige rij(en) verwijdert en het grid/vorige 
        /// tetromino's poisitie aanpast
        /// </summary>
        /// <param name="completeRijen"></param>
        private void ControleCompleteRijen(bool[] completeRijen)
        {
            bool[] verschillend = completeRijen.Distinct().ToArray();
            if (verschillend.Length == 2)
            {
                VerwijderCompleteRijen(completeRijen);
            }
        }

        //Verwijdert de blokjes die weg mogen (zowel uit canvas als uit de blokjes list)
        //Date: 27/03/2015 11:00
        //Auteur: Kim Gijbels
        /// <summary>
        /// Methode die de blokjes verwijdert die op de posities staan van de volledige rij(en)
        /// Blokjes worden zowel verwijderd uit canvas als uit de vorige tetromino(es)
        /// </summary>
        /// <param name="tetromino"></param>
        /// <param name="row"></param>
        private void DeleteBlokjes(int tetromino, int row)
        {
            List<int> verwijderbareBlokjes = new List<int>();

            for (int blokje = vorigeTetrominoes[tetromino].Tetromino.AantalBlokjes - 1; blokje >= 0; blokje--)
            {
                if (vorigeTetrominoes[tetromino].Tetromino.GetYPosBlokje(blokje) / (int)Blokjes.Zijde == row)
                {
                    vorigeTetrominoes[tetromino].Tetromino.VerwijderBlokje(blokje, gridCanvas);
                    verwijderbareBlokjes.Add(blokje);
                }
            }
            vorigeTetrominoes[tetromino].Tetromino.VerwijderBlokjesUitList(verwijderbareBlokjes);
        }

        //Detecteert welke rijen onvolledig zijn => het is veel makkelijker om te doen alsof ze allemaal vol zijn,
        //want één observatie is voldoende om het tegendeel te bewijzen
        //Date: 06/04/2015 16:01
        //Auteur: Kim Gijbels
        /// <summary>
        /// Detecteert of er rijen zijn die onvolledig zijn
        /// </summary>
        /// <param name="completeRijen"></param>
        /// <returns></returns>
        private bool[] DetecteertOncompleteRijen(bool[] completeRijen)
        {
            //Detecteert waar de rijen niet volledig zijn
            for (int rij = 0; rij < grid.Count; rij++)
            {
                bool onvolledigeRij = false;
                for (int kolom = 0; kolom < grid[rij].Length && !onvolledigeRij; kolom++)
                {
                    if (!grid[rij][kolom])
                    {
                        completeRijen[rij] = false;
                        onvolledigeRij = true;
                    }
                }
            }
            return completeRijen;
        }

        //Tekent de eindlijn -> Als blokjes deze lijn raken, dan is het game-over
        //Date: 06/04/2015  20:07
        //Auteur: Kim Gijbels
        /// <summary>
        /// Tekent de eindlijn op het canvas
        /// </summary>
        private void DrawEndingLineOnCanvas()
        {
            Line endingLine = new Line();
            endingLine.X1 = 0;
            endingLine.X2 = gridCanvas.Width;
            endingLine.Y1 = 60;
            endingLine.Y2 = 60;
            endingLine.Stroke = new SolidColorBrush(Colors.Crimson);
            endingLine.StrokeThickness = 3;
            gridCanvas.Children.Add(endingLine);
        }

        //Kijkt of er blokjes op de lijn zitten vooraleer dat er een nieuw blokje gegenereerd wordt
        //Date 06/04/2015 20:54
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controleert of blokjes de eindstreep raken, zo ja= game over= true
        /// </summary>
        /// <returns></returns>
        private bool GameOver()
        {
            for (int col = 0; col < grid[2].Length; col++)
            {
                if (grid[2][col])
                {
                    MessageBox.Show("Game Over! Je score is: " + score);
                    return true;
                }
            }
            return false;
        }

        //Genereert een niew tetrisblokje als het spel niet verloren is en start de tijd opnieuw
        //Date: 28/03/2015 10:45
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controleert of er een nieuw blokje gegenereert mag worden
        /// Zoniet: het spel wordt afgesloten
        /// </summary>
        private void GenereerRandomTetromino()
        {
            if (!GameOver())
            {
                int nummer = randomGetalGen.Next(7);
                huidigeTetromino = new Tetrisvorm(InitialX, InitialY, nummer, brushes[nummer]);
                valTijd.Start();
                huidigeTetromino.UpdateCanvas(gridCanvas);
            }
            else
            {
                Afsluiten();
                window.Close();
            }
        }

        //Verwijdert eerst de volledige rijen uit het grid, en voegt daarna even veel false array's toe als er rijen verwijderd werden.
        //Hierdoor verplaatsen de andere rijen zich x plaatsen naar beneden
        //Date: 30/03/2015 18:45
        //Auteur: Kim Gijbels
        /// <summary>
        /// Herziet het grid als er een volledige rij gedetecteerd werd en er blokjes verwijderd werden
        /// en vorige blokjes naar beneden werden geplaatst
        /// </summary>
        /// <param name="vervolledigdeRijen"></param>
        private void GridHerzien(List<int> vervolledigdeRijen)
        {
            //Volgorde is super belangrijk: het verwijderen klopt maar als je er niets ondertussen aan toevoegt, want dan 
            //shiften de rijen van index!
            for (int i = 0; i < vervolledigdeRijen.Count; i++)
            {
                grid.RemoveAt(vervolledigdeRijen[i]);
            }
            for (int i = 0; i < vervolledigdeRijen.Count; i++)
            {
                vulArray = new bool[(int)gridCanvas.Width / (int)Blokjes.Zijde];
                grid.Insert(0, vulArray);
            }
        }

        //Initialiseert het grid op false => nog geen blokjes gevallen. Grid is een herschaalde (kleinere) versie van het speelveld => grootte van de blokjes
        //Date: 02/05/2015 11:03
        //Auteur: Kim Gijbels
        /// <summary>
        /// Initieel instellen van het grid op false
        /// </summary>
        private void InitialiseerGrid()
        {
            for (int i = 0; i < (int)(gridCanvas.Height / Blokjes.Zijde); i++)
            {
                vulArray = new bool[(int)gridCanvas.Width / (int)Blokjes.Zijde];
                //Dit moet hier steeds opnieuw gemaakt worden, anders is het een reference type -> verwijs naar één en de rest past mee aan
                grid.Add(vulArray);
            }
        }

        //Initialiseert de volle rijen eerst op true, het is namelijk gemakkeijker om te detecteren welke niet vol zijn -> een ontbrekende kolom -> onvolledige rij
        //Date: 06/04/2015 Date: 16:00
        //Auteur: Kim Gijbels
        /// <summary>
        /// Initialisatie van completeRijen op volledig
        /// </summary>
        /// <param name="completeRijen"></param>
        /// <returns></returns>
        private bool[] InitialiseertCompleteRijen(bool[] completeRijen)
        {
            for (int row = 0; row < completeRijen.GetLength(0); row++) //Initialiseren van complete rijen
            {
                completeRijen[row] = true;
            }
            return completeRijen;
        }

        //Kijkt of het huidige blokje op de grond staat (aangezien dat er in C# getekend wordt vanaf de linkerbovenhoek, moet er nog een zijde worden bijgeteld om te kijken of het op de grond staat)
        //Date: 27/03/2015 16:00
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controleert of het blokje de grond geraakt heeft
        /// </summary>
        /// <returns></returns>
        private bool IsBlokjeOpGrond()
        {
            if (huidigeTetromino.LaagstHangendBlokjeYPos + Blokjes.Zijde >= gridCanvas.Height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Controleert of de extreme blokjes dooor een rotatie niet uit het canvas zouden vallen, eerste check van rotatie
        //Date: 06/04/2015 16:10
        //Auteur: Kim Gijbels
        /// <summary>
        /// Simuleert de rotatie en controleert of er door de rotatie één van de blokjes over de randen van het canvas gaan
        /// </summary>
        /// <returns></returns>
        private bool IsRotatieOverCanvas()
        {
            double xRechts, yOnder, xLinks, yBoven;
            double rotatieRechts, rotatieLinks, rotatieOnder, rotatieBoven;

            xRechts = -huidigeTetromino.Tetromino.GetYPosRelToRotatiePunt(huidigeTetromino.Tetromino.IndexHoogsteBlokje);
            rotatieRechts = (huidigeTetromino.Tetromino.XRotationPoint + xRechts) - (Blokjes.Zijde / 2);

            xLinks = -huidigeTetromino.Tetromino.GetYPosRelToRotatiePunt(huidigeTetromino.Tetromino.IndexLaagsteBlokje);
            rotatieLinks = (huidigeTetromino.Tetromino.XRotationPoint + xLinks) - (Blokjes.Zijde / 2);

            yOnder = huidigeTetromino.Tetromino.GetXPosRelToRotatiePunt(huidigeTetromino.Tetromino.IndexRechtsteBlokje);
            rotatieOnder = (huidigeTetromino.Tetromino.YRotationPoint + yOnder) - (Blokjes.Zijde / 2);

            yBoven = huidigeTetromino.Tetromino.GetXPosRelToRotatiePunt(huidigeTetromino.Tetromino.IndexLinksteBlokje);
            rotatieBoven = (huidigeTetromino.Tetromino.YRotationPoint + yBoven) - (Blokjes.Zijde / 2);

            if (rotatieRechts >= gridCanvas.Width || rotatieLinks < 0 || rotatieOnder >= gridCanvas.Height ||
                rotatieBoven < 0)
            {
                return true;
            }
            return false;
        }

        //Controleert of een huidig blokje van een tetromino over een andere tetromino zou vallen
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controle of vallend blokje een andere tetromino raakt
        /// </summary>
        /// <returns></returns>
        private bool IsVolgendePosOverlap()
        {
            if (vorigeTetrominoes.Count != 0)
            //Heeft geen zin om te controleren als er geen tetrominos op het speelveld staan
            {
                if (grid[
                        ((int)huidigeTetromino.Tetromino.GetYPosBlokje(0) + (int)Blokjes.Zijde) /
                        (int)Blokjes.Zijde][(int)huidigeTetromino.Tetromino.GetXPosBlokje(0) / (int)Blokjes.Zijde]
                    ||
                    grid[
                            ((int)huidigeTetromino.Tetromino.GetYPosBlokje(1) + (int)Blokjes.Zijde) /
                            (int)Blokjes.Zijde][(int)huidigeTetromino.Tetromino.GetXPosBlokje(1) / (int)Blokjes.Zijde
                            ]
                    ||
                    grid[
                            ((int)huidigeTetromino.Tetromino.GetYPosBlokje(2) + (int)Blokjes.Zijde) /
                            (int)Blokjes.Zijde][(int)huidigeTetromino.Tetromino.GetXPosBlokje(2) / (int)Blokjes.Zijde
                            ]
                    ||
                    grid[
                            ((int)huidigeTetromino.Tetromino.GetYPosBlokje(3) + (int)Blokjes.Zijde) /
                            (int)Blokjes.Zijde][(int)huidigeTetromino.Tetromino.GetXPosBlokje(3) / (int)Blokjes.Zijde
                            ])
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        //Controleert of dat het huidige blokje over een ander blokje heen zou gaan moest het horizontaal bewegen
        //Date: 28/03/2015 11:50
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controle of blokje over een ander blokje gaat tijdens een horizontale beweging
        /// </summary>
        /// <param name="richting"></param>
        /// <returns></returns>
        private bool IsVolgendePosOverlapHor(int richting)
        {
            if (vorigeTetrominoes.Count != 0)
            //Het heeft geen zin om verder te kijken als er geen tetrominos in het spel zitten
            {
                if (grid[(int)huidigeTetromino.Tetromino.GetYPosBlokje(0) / (int)Blokjes.Zijde][
                        ((int)huidigeTetromino.Tetromino.GetXPosBlokje(0) + (int)(Blokjes.Zijde * richting)) /
                        (int)Blokjes.Zijde]
                    ||
                    grid[(int)huidigeTetromino.Tetromino.GetYPosBlokje(1) / (int)Blokjes.Zijde][
                            ((int)huidigeTetromino.Tetromino.GetXPosBlokje(1) + (int)(Blokjes.Zijde * richting)) /
                            (int)Blokjes.Zijde]
                    ||
                    grid[(int)huidigeTetromino.Tetromino.GetYPosBlokje(2) / (int)Blokjes.Zijde][
                            ((int)huidigeTetromino.Tetromino.GetXPosBlokje(2) + (int)(Blokjes.Zijde * richting)) /
                            (int)Blokjes.Zijde]
                    ||
                    grid[(int)huidigeTetromino.Tetromino.GetYPosBlokje(3) / (int)Blokjes.Zijde][
                            ((int)huidigeTetromino.Tetromino.GetXPosBlokje(3) + (int)(Blokjes.Zijde * richting)) /
                            (int)Blokjes.Zijde])
                {
                    return true;
                }
            }
            return false;
        }

        //Controleert of de extreme blokjes (blokjes die aan de buitenkant zitten) over de rand van de canvas zouden gaan
        //Richting: -1 = links, 1 = rechts
        //Datum: 28/03/2015 11:55
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controle bij het horizontaal bewegen of tetromino over de grenzen van het canvas heen zouden gaan
        /// </summary>
        /// <param name="richting"></param>
        /// <returns></returns>
        private bool IsVolgendePosOverCanvas(int richting)
        {
            if (huidigeTetromino.Tetromino.RechtseBlokXPos + Blokjes.Zijde * richting + Blokjes.Zijde <=
                gridCanvas.Width
                && huidigeTetromino.Tetromino.LinkseBlokXPos + Blokjes.Zijde * richting >= 0)
            {
                return false;
            }
            return true;
        }

        //Level systeem, zelf bedacht
        //Date: 06/04/2015 20:37
        //Auteur: Kim Gijbels
        /// <summary>
        /// Controle of er een level omhoog gegaan mag worden, op basis van clearedLines
        /// </summary>
        private void LevelUp()
        {
            if ((verwijderdeLijnen - (5 * (level + 1)) >= 0))
            {
                level++;
                if (timingInterval - 50 > 100)
                {
                    timingInterval -= 50;
                    valTijd.Interval = TimeSpan.FromMilliseconds(timingInterval);
                }
                verwijderdeLijnen = 0;
                levelLabel.Content = string.Format("Level {0:D2}", level);
            }
        }

        //Default waarden van de kleurtjes voor de tetrominoes
        //Date: 06/04/2015 15:12
        //Auteur: Kim Gijbels
        /// <summary>
        /// Zet de default waarden voor de tetrominoes
        /// </summary>
        private void SetBrushColors()
        {
            brushes[0] = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            brushes[1] = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            brushes[2] = new SolidColorBrush(Color.FromRgb(0, 128, 255));
            brushes[3] = new SolidColorBrush(Color.FromRgb(127, 0, 255));
            brushes[4] = new SolidColorBrush(Color.FromRgb(255, 0, 127));
            brushes[5] = new SolidColorBrush(Color.FromRgb(255, 255, 0));
            brushes[6] = new SolidColorBrush(Color.FromRgb(0, 255, 255));
        }

        //Tekent een boord rond het canvas, zodat het duidelijk is wat nog deel uit maakt van het canvas en wat niet
        //Date: 08/05/2015 13:24
        //Auteur: Kim Gijbels
        /// <summary>
        /// Voegt een boord toe aan het canvas
        /// </summary>
        private void TekenBoordRondCanvas()
        {
            Border canvasBorder = new Border();
            canvasBorder.BorderBrush = Brushes.Blue;
            canvasBorder.BorderThickness = new Thickness(4);
            canvasBorder.Width = gridCanvas.Width + 4; //omwille van de thickness
            canvasBorder.Height = gridCanvas.Height + 4; //omwille van de thickness
            gridCanvas.Children.Add(canvasBorder);
        }

        //Als er geen highscore is of de highscore is kleiner dan de score, dan wordt de highscore samen met de score geüpdatet
        //Date 06/04/2015 20:49
        //Auteur: Kim Gijbels
        /// <summary>
        /// Update de highscore als het nodig is
        /// </summary>
        private void UpdateHighScore()
        {
            if (Convert.ToUInt32(highscoreLabel.Content) < score)
            {
                highscoreLabel.Content = string.Format("{0:D7}", score);
                Bestandenverwerker.ControleHogereTetrisHighscore(UInt32.Parse(highscoreLabel.Content.ToString()));
            }
        }

        //Berkent de score als er (een) complete rij(en) is/zijn http://www.gamedev.net/topic/290296-does-anyone-know-how-the-scoring-system-works-in-tetris/
        //Date: 06/04/2015 20:30
        //Auteur: Kim Gijbels
        /// <summary>
        /// Score systeem van tetris
        /// </summary>
        /// <param name="aantalCompleteRijen"></param>
        private void UpdateScore(int aantalCompleteRijen)
        {
            verwijderdeLijnen += aantalCompleteRijen;
            switch (aantalCompleteRijen)
            {
                case 1:
                    score += (uint)(50 * (level + 1));
                    break;
                case 2:
                    score += (uint)(150 * (level + 1));
                    break;
                case 3:
                    score += (uint)(350 * (level + 1));
                    break;
                default:
                    score += (uint)(1000 * (level + 1));
                    break;
            }
            scoreLabel.Content = string.Format("{0:D7}", score);
            UpdateHighScore();
        }

        //Elke vooraf ingestelde tijd moet een blokje naar beneden vallen, doorheen de levels gaat dit steeds sneller
        //Date: 27/03/2015 13:12
        //Auteur: Kim Gijbels
        /// <summary>
        /// Valsnelheid van tetrominoes, roept de beweeg naar beneden methode aan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void valTijd_Tick(object sender, EventArgs e)
        {
            BeweegNaarBeneden();
        }

        //Wanneer een rij verwijderd werd, moet er voor gezorgd worden dat de rest van de blokjes naar beneden verschuiven
        //Date: 27/03/2015 11:35
        //Auteur: Kim Gijbels
        /// <summary>
        /// Verschuift de blokjes naar beneden als er een rij verwijderd werd.
        /// Zorgt er ook voor dat het grid mee aangepast wordt
        /// </summary>
        /// <param name="vervolledigdeRijen"></param>
        private void VerlaagAlleTetrominoes(List<int> vervolledigdeRijen)
        {
            Thickness benedenVerplaatsing = new Thickness(0, Blokjes.Zijde, 0, 0);

            for (int tetromino = 0; tetromino < vorigeTetrominoes.Count; tetromino++)
            {
                for (int blokje = 0; blokje < vorigeTetrominoes[tetromino].Tetromino.AantalBlokjes; blokje++)
                {
                    for (int completeRij = vervolledigdeRijen.Count - 1; completeRij >= 0; completeRij--)
                    {
                        if (vorigeTetrominoes[tetromino].Tetromino.GetYPosBlokje(blokje) / Blokjes.Zijde <
                            vervolledigdeRijen[completeRij])
                        {
                            vorigeTetrominoes[tetromino].Tetromino.SetTopMarginBlokje(blokje, benedenVerplaatsing);
                        }
                    }
                }
            }
            GridHerzien(vervolledigdeRijen);
        }

        //Zoekt waar de complete rijen zich bevinden en verwijdert deze dan
        //Date: 27/03/2015 15:02
        //Auteur: Kim Gijbels
        /// <summary>
        /// Zoekt de complete rijen en verwijdert roept de benodigde methodes dan aan om ze te verwijderen
        /// </summary>
        /// <param name="completeRijen"></param>
        private void VerwijderCompleteRijen(bool[] completeRijen)
        {
            List<int> vervolledigdeRijen = new List<int>();

            for (int rij = grid.Count - 1; rij >= 0; rij--)
            {
                if (completeRijen[rij])
                {
                    vervolledigdeRijen.Add(rij);

                    for (int tetromino = 0; tetromino < vorigeTetrominoes.Count; tetromino++)
                    {
                        DeleteBlokjes(tetromino, rij);
                    }
                }
            }
            VerwijderVorigeTetrominoes();
            VerlaagAlleTetrominoes(vervolledigdeRijen);
            UpdateScore(vervolledigdeRijen.Count);
            LevelUp();
        }

        //Als de vorige tetrominoes geen blokjes meer bevatten worden ze uit de lijst van vorigeTetrominoes gegooid
        //Date: 28/03/2015 11:01
        //Auteur: Kim Gijbels
        /// <summary>
        /// Verwijdert de tetrominoes als ze geen blokjes meer hebben
        /// </summary>
        private void VerwijderVorigeTetrominoes()
        {
            for (int tetromino = 0; tetromino < vorigeTetrominoes.Count; tetromino++)
            {
                if (vorigeTetrominoes[tetromino].Tetromino.AantalBlokjes == 0)
                {
                    vorigeTetrominoes.RemoveAt(tetromino);
                }
            }
        }

        //Voert de rotatie uit, als de checks goed uitgevoerd werden en hercalculeert de posities extreme blokjes (blokjes die aan de buitenkant zitten)
        //Date: 06/04/2015 16:15
        //Auteur: Kim Gijbels
        /// <summary>
        /// Voert de rotaie uit en berekent de nieuwe posities van de blokjes
        /// </summary>
        /// <param name="rotatieCoordinaten"></param>
        private void VoerRotatieUit(Thickness[] rotatieCoordinaten)
        {
            for (int blokje = 0; blokje < huidigeTetromino.Tetromino.AantalBlokjes; blokje++)
            {
                huidigeTetromino.Tetromino.UpdatePosBlokje(blokje, rotatieCoordinaten[blokje]);
            }
            huidigeTetromino.Tetromino.GetExtremePosBlokjes();
        }

        //Vult het grid dat gebruikt zal worden om te controleren of een vallend/verplaatsend blokje bovenop een ander blokje zou gaan
        //Date: 27/03/15:07
        //Auteur: Kim Gijbels
        /// <summary>
        /// Wanneer een blokje niet meer verder naar beneden kan vallen, wordt de positie geregistreerd in het grid
        /// </summary>
        private void VullenGrid()
        {
            for (int blokje = 0; blokje < huidigeTetromino.Tetromino.AantalBlokjes; blokje++)
            {
                grid[(int)huidigeTetromino.Tetromino.GetYPosBlokje(blokje) / (int)Blokjes.Zijde][
                        (int)huidigeTetromino.Tetromino.GetXPosBlokje(blokje) / (int)Blokjes.Zijde] = true;
            }
        }

        //De event handlers van het spel
        //Datum: 27/03/2015 12:15
        //Auteur: Kim Gijbels
        /// <summary>
        /// Event Handlers voor het tetrisspel 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                BeweegNaarBeneden();
            }
            else if (e.Key == Key.Left)
            {
                BeweegHorizontaal(-1);
                //-1 geeft de richting aan, x waardes moeten verminderen als er naar links wordt gegaan, vandaar -1
            }
            else if (e.Key == Key.Right)
            {
                BeweegHorizontaal(1);
                //1 geeft de richting aan, x waardes moeten verhoogt worden als het blokje naar rechts beweegt
            }
            else if (e.Key == Key.Space)
            {
                RoteerTetromino();
            }
        }
    }
}