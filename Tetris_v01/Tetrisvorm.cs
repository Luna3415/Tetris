﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tetris_v01
{
    //Maakt de tetrisvormen aan en nodig voor toegang tot de blokjes
    class Tetrisvorm
    {
        private Blokjes tetromino;

        //Constructor voor de tetromino klaar te maken (positionering, kleur)
        //Date: 27/03/2015 
        //Auteur: Kim Gijbels
        public Tetrisvorm(double x, double y, int welkeVorm, SolidColorBrush brush)
        {
            tetromino = new Blokjes(welkeVorm, x, y, brush);
        }

        //Hiermee is er toegang tot de blokjes
        //Date: 27/03/2015 14:00
        //Auteur: Kim Gijbels
        public Blokjes Tetromino
        {
            get { return tetromino; }
        }

        //Nodig voor controle uit te voeren tijdens het tetrisspel
        //Date: 27/03/2015 14:20
        //Auteur: Kim Gijbels
        public double LaagstHangendBlokjeYPos
        {
            get { return tetromino.LaagstHangendYPos; }
        }

        //Zorgt ervoor dat nieuwe tetromino's toegevoegd kunnen worden aan het speelveld
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void UpdateCanvas(Canvas gridCanvas)
        {
            for (int blokje = 0; blokje < tetromino.AantalBlokjes; blokje++)
            {
                gridCanvas.Children.Add(tetromino.GetBlokje(blokje));
            }
        }

        //Telt een aantal y-waardes bij de huidige y-waarde van elk blokje, om zo het blokje naar beneden te laten vallen
        //Date: 27/03/2015
        //Auteur: Kim Gijbels
        public void SetTopMarginTetromino(Thickness positie)
        {
            for (int blokje=0; blokje<tetromino.AantalBlokjes; blokje++ )
            {
                Thickness margin = tetromino.GetBlokje(blokje).Margin;
                margin.Top += positie.Top;
                tetromino.GetBlokje(blokje).Margin = margin;
            }
        }

        //Telt een aantal x-waardes bij de huidige x-waarde van elk blokje, om zo het blokje horizontaal te kunnen verplaatsen (in geval van links, zal er een negatieve x worden bijgeteld)
        //Date: 28/03/2015
        //Auteur: Kim Gijbels
        public void SetLeftMarginTetromino(Thickness positie)
        {
            for (int blokje = 0; blokje < tetromino.AantalBlokjes; blokje++)
            {
                Thickness margin = tetromino.GetBlokje(blokje).Margin;
                margin.Left += positie.Left;
                tetromino.GetBlokje(blokje).Margin = margin;
            }
        }
    }
}
